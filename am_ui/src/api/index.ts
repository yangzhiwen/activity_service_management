import axios from 'axios'

axios.defaults.withCredentials = true;
const api = axios.create({
  timeout: 10000,
  responseType: 'json',
  withCredentials: true
})

api.interceptors.request.use(
  request => {
    const token = localStorage.getItem('token') || null
    if (token != null) {
      request.headers = {
        'content-Type': 'application/json;charset=utf-8',
        token: token
      }
    }
    return request
  }
)

api.interceptors.response.use(
  response => {
    console.log(response);

    const data = response.data

    if (data.code == 400) {
      localStorage.removeItem('userDataCache')
      // window.location.href = '/login'

    }
    return Promise.resolve(data)
  },
  error => {
    console.log(error);

    return Promise.reject(error)
  }
)

export default api