import api from '@/api/index'

export const login = (data: any) => {
  return api.post('/api/user/login', data)
}

export const newsSelectAll = () => {

  return api.get('/api/news/selectAll')
}
export const newsSearchByName = (title: string) => {

  return api.get(`/api/news/searchByName/${title}`)
}
export const newsSearchCondition = (data: any) => {

  return api.post('/api/news/searchByCondition', data)
}
export const newsSelectByMonthDate = (deptId: number, date: string) => {
  let data = {
    departmentId: deptId,
    date: date
  }
  return api.post(`/api/news/selectByMonthDate`, data)
}
export const newsDelete = (id: number) => {

  return api.delete(`/api/news/delete//${id}`)
}
export const newsExamine = (id: number) => {
  return api.get(`/api/news/examine/${id}`)
}
export const newsReject = (id: number) => {
  return api.get(`/api/news/reject/${id}`)
}
export const newsClicks = (id: number) => {
  return api.get(`/api/news/clicks/${id}`)
}
export const newsSave = (data: any) => {

  return api.post('/api/news/save', data)
}

export const categorySelectAll = () => {
  return api.get('/api/category/selectAll')
}
export const categoryDelete = (id: number) => {

  return api.delete(`/api/category/delete/${id}`)
}
export const categorySave = (data: any) => {
  return api.post('/api/category/save', data)
}
export const commentsInsert = (data: any) => {
  return api.post('/api/comments/insert', data)
}

export const commentsSelectByNewsId = (id: number) => {

  return api.get(`/api/comments/selectCommentsById/ ${id}`)
}
export const uploadSelectByNewsId = (id: number) => {

  return api.get(`/api/upload/getResourceById/${id}`)
}
export const uploadDeleteById = (id: number) => {

  return api.get(`/api/upload/deleteById/${id}`)
}


// 查询所有部门
export const departmentSelectAll = () => {

  return api.get('/api/department/selectAll')
}

//删除部门
export const departmentDelete = (id: number) => {

  return api.delete(`/api/department/delete/${id}`)
}

export const departmentSave = (data: any) => {
  return api.post('/api/department/save', data)
}

//更改密码/user/updatePwd/{username}/{oldPwd}/{newPwd}
export const userUpdate = (username: string, olePwd: string, newPwd: string) => {
  return api.post(`/api/user/updatePwd/${username}/${olePwd}/${newPwd}`)
}



//增加用户http://172.18.4.63:8088/user/selectAll
export const userSelectAll = () => {
  return api.get('/api/user/selectAll')
}

//删除用户/user/deleteByID/{ID}
export const userDeleteByID = (id: number) => {
  return api.post(`/api/user/deleteByID/${id}`)
}
export const resetUserUpdate = (id: number) => {
  return api.post(`/api/user/resetUserUpdate/${id}`)
}


//获得部门和对应id/department/selectAll
export const getDepartmentSelectAll = () => {
  return api.get('/api/department/selectAll')
}

//增加用户 POST/user/insert
export const addUser = (data: any) => {
  return api.post('/api/user/insert', data)
}

//根据用户修改部门/user/updateByName
export const updateUserDepartment = (data: any) => {
  return api.post('/api/user/updateByName', data)
}


//日志模块/log/findAll
export const logfindAll = () => {
  return api.get('/api/log/findAll')
}

//日志模块deleteLogById
export const logDeleteById = (data: any) => {
  return api.post('/api/log/deleteLogById', data)
}
//日志模块分页1
export const logPage = (current: number, size: number) => {
  return api.get(`/api/log/findAllPage/${current}/${size}`)
}
