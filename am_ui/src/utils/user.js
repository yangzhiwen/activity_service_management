export const getUserLevel = () => {
  const userDataCache = getUserDataCache()
  if (userDataCache != null) {
    return userDataCache.userlevel
  }
  return 4


}


export const getDeptId = () => {
  const userDataCache = getUserDataCache()
  if (userDataCache != null) {
    return userDataCache.departmentid
  }

  return 1
}



export const getUserDataCache = () => {
  const userDataTemp = localStorage.getItem('userDataCache')
  if (userDataTemp != null) {
    const userDataJson = JSON.parse(userDataTemp) || null
    if (userDataJson != null) {
      return userDataJson
    }
  }
  return null
}