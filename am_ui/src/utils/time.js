export let rTime = (date) => {
  let json_date = new Date(date).toJSON();
  return new Date(new Date(json_date).getTime() + 8 * 3600 * 1000).toISOString().replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '').toString().substr(0, 10)
}

export let parseWeekData = data => {
  if (data.week == null) {
    return JSON.parse(data)
  }
  return data
}

export let getWeekByNum = num => {

  const week = [
    '',
    '星期日',
    '星期一',
    '星期二',
    '星期三',
    '星期四',
    '星期五',
    '星期六',

  ]
  return week[num]


}




