import Vue from 'vue'
import VueRouter, { NavigationGuardNext, Route, RouteConfig } from 'vue-router'
import { matchRouteMenu } from './../../matchRouteMenu'
/* Layout */
import Layout from '@/views/layout/index.vue'

Vue.use(VueRouter)

export const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'index',
    component: () => import('@/index.vue'),
    meta: {
      hidden: true
    }
  },
  {
    path: '/admin',
    name: 'Layout',
    component: Layout,
    redirect: '/admin/index',
    children: [
      {
        path: 'index',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/About.vue'),
        name: 'index',
        meta: {
          title: '首页',
          icon: 'dashboard'
        }
      }
    ]
  },
  {
    path: '/Login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "Login" */ '@/views/pages/login/index.vue'),
    meta: {
      hidden: true
    }
  },

  // {
  //   path: '/*',
  //   component: () => import(/* webpackChunkName: "404" */ '@/views/pages/error-page/404.vue'),
  //   meta: { hidden: true }
  // },
]

const userDataTemp = localStorage.getItem('userDataCache') || null
let userLevel = 4


if (userDataTemp != null) {
  const userDataJson = JSON.parse(userDataTemp) || null
  if (userDataJson != null) {
    userLevel = userDataJson.userlevel
  }

}
console.log(userLevel);





const activePage = {
  path: '/news',
  component: Layout,
  redirect: '/news/manage',
  meta: {
    title: '活动页面',
    icon: 'list'
  },
  children: [
    {
      path: '/news/manage',
      name: 'news-manage',
      component: () => import('@/views/pages/news/news-manage.vue'),
      meta: {
        title: '活动管理',
        icon: 'edit',
        noCache: false
      }
    }

  ]
}

if (userLevel == 1 || userLevel == 2 || userLevel == 3) {
  if(userLevel == 1){
    activePage.children.push({
      path: '/news/type',
      name: 'news-type',
      component: () => import('@/views/pages/news/news-type.vue'),
      meta: {
        title: '活动类型',
        icon: 'edit',
        noCache: false
      }
    })

  }
  routes.push(activePage)

}



const departmentPage = {
  path: '/department',
  component: Layout,
  redirect: '/department/manage',
  meta: {
    title: '部门页面',
    icon: 'list'
  },
  children: [
    {
      path: '/department/manage',
      name: 'department-manage',
      component: () => import('@/views/pages/department/department-manage.vue'),
      meta: {
        title: '部门管理',
        icon: 'willDoTask',
        noCache: false
      }
    }

  ]
}

if (userLevel == 1) {
  routes.push(departmentPage)
}


const personalSettingPage = {
  path: '/user',
  component: Layout,
  redirect: '/user/updatePwd',
  meta: {
    title: '个人信息',
    icon: 'list'
  },
  children: [
    {
      path: '/user/updatePwd',
      name: 'user-updatePwd',
      component: () => import('@/views/pages/personalSetting/personal-UpdatePwd.vue'),
      meta: {
        title: '修改密码',
        icon: 'password',
        noCache: false
      }
    }

  ]
}

if (userLevel == 1 || userLevel == 2 || userLevel == 3) {
  routes.push(personalSettingPage)
}



const usersPage = {
  path: '/users',
  component: Layout,
  redirect: '/users/manage',
  meta: {
    title: '用户页面',
    icon: 'user'
  },
  children: [
    {
      path: '/users/manage',
      name: 'users-manage',
      component: () => import('@/views/pages/users/users-manage.vue'),
      meta: {
        title: '用户管理',
        icon: 'user',
        noCache: false
      }
    },

  ]
}

if (userLevel == 1) {
  routes.push(usersPage)

}

//日志管理页面
const logPage = {
  path: '/log',
  component: Layout,
  redirect: '/log/manage',
  meta: {
    title: '日志页面',
    icon: 'phone'
  },
  children: [
    {
      path: '/log/manage',
      name: 'log-manage',
      component: () => import('@/views/pages/log/log-manage.vue'),
      meta: {
        title: '日志管理',
        icon: 'willDoTask',
        noCache: false
      }
    },

  ]
}
console.log(userLevel);

if (userLevel == 1) {
  routes.push(logPage)
}






const router = new VueRouter({
  mode: 'history',
  routes
})
// 路由守卫
router.beforeEach((to: Route, from: Route, next: NavigationGuardNext<Vue>) => {
  matchRouteMenu(to, from, next);
})

export default router


