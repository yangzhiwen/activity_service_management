package com.activity.common.constact;

/**
 * @version V1.0
 * @Package com.activity.common.constact
 * @auhter 枯木Kum
 * @date 2021/5/24-3:47 PM
 * <p>...</p>
 */
public class UserLevel {


    /**
     * 管理员
     */
    public static final Integer ADMIN = 1;
    /**
     * 活动审核员
     */
    public static final Integer NEWS_EXAMINE_USER = 2;
    /**
     * 活动发布员
     */
    public static final Integer NEWS_ADD = 3;
    /**
     * 普通用户
     */
    public static final Integer USER = 4;


}
