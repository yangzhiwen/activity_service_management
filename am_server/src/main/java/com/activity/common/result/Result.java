package com.activity.common.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private String msg;
    private boolean status;
    private Object data;
    private Integer code=200;

    public Result(String msg, boolean status, Object data) {
        this.msg = msg;
        this.status = status;
        this.data = data;
    }

    public static Result success(String message){
        return new Result(message,true,null);
    }
    public static Result success(String message, Object data){
        return new Result(message,true,data);
    }
    public static Result fail(String message){
        return new Result(message,false,null);
    }
    public static Result fail(String message,Object data){
        return new Result(message,false,data,400);
    }

}
