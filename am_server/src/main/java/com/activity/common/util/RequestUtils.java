package com.activity.common.util;

import com.alibaba.fastjson.JSON;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @version V1.0
 * @Package com.kum.utils
 * @auhter 枯木Kum
 * @date 2021/3/19-4:32 PM
 */

@Log4j2
public class RequestUtils {

    private static Map<String, Object> SessionsDataMap = new HashMap<>();


    /**
     * 获取当前线程的Request对象
     *
     * @return
     */
    public static HttpServletRequest getCurrentRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return requestAttributes.getRequest();
    }

    /**
     * 获取当前线程的Response对象
     *
     * @return
     */
    public static HttpServletResponse getCurrentResponse() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return requestAttributes.getResponse();
    }

    public static void setCurrentSessionAttribute(String key, Object val) {
        getCurrentRequest().getSession().setAttribute(key, val);
    }

    public static void removeCurrentSessionAttribute(String key) {
        getCurrentRequest().getSession().removeAttribute(key);
    }

    public static Object getCurrentSessionAttribute(String key) {
        return getCurrentRequest().getSession().getAttribute(key);
    }

    public static String getToken() {
        return getCurrentRequest().getHeader("token");
    }

    public static String getCurrentUsername() {
        return JwtUtil.getPayloadd(getToken()).getString("username");
    }

    public static Integer getCurrentUserLevel() {
        return JwtUtil.getPayloadd(getToken()).getInteger("userlevel");
    }


}
