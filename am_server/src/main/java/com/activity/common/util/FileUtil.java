package com.activity.common.util;

import com.activity.entity.Resourceurl;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;

public class FileUtil {
    public void showFileImg( HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("image/png");//显示图片
       // response.setContentType("application/ogg");//显示视频
        //获取图片
        File file = new File("D:/activityResourceUrl/02326380-17d3-4949-9dc4-0a24d9de5771.mp4");//存放文件名
        //创建文件输入流
        FileInputStream is = new FileInputStream(file);
        // 响应输出流
        ServletOutputStream out = response.getOutputStream();
        // 创建缓冲区
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = is.read(buffer)) != -1) {
            out.write(buffer, 0, len);
        }
        is.close();
        out.flush();
        out.close();
        }


}
