package com.activity.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

public class JwtUtil {
    /**
     * 过期时间为一天
     * TODO 正式上线更换为15分钟
     */
    private static final long EXPIRE_TIME = 12 * 60 * 60 * 1000;

    /**
     * token私钥
     */
    private static final String TOKEN_SECRET = "2333";

    /**
     * 生成签名,15分钟后过期
     *
     * @param username
     * @param
     * @return
     */
    public static String sign(String username,Integer userlevel) {
        //过期时间
        Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
        //私钥及加密算法
        Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
        //设置头信息
        HashMap<String, Object> header = new HashMap<>(2);
        header.put("typ", "JWT");
        header.put("alg", "HS256");
        return JWT.create().withHeader(header)
                .withClaim("username", username)
                .withClaim("userlevel", userlevel)
                .withExpiresAt(date).sign(algorithm);
    }

    //验证 token 是否有效
    public static boolean verity(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    //获得 token 中的载荷信息
    public JSONObject getPayload(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            String payload = jwt.getPayload();

            return (JSONObject) JSON.parse(Base64.getDecoder().decode(payload));

        } catch (IllegalArgumentException | JWTVerificationException e) {
            return null;
        }
    }
    //获得 token 中的载荷信息
    public static JSONObject getPayloadd(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            String payload = jwt.getPayload();

            return (JSONObject) JSON.parse(Base64.getDecoder().decode(payload));

        } catch (IllegalArgumentException | JWTVerificationException e) {
            return null;
        }
    }


    public static void main(String[] args) {

    }
}
