package com.activity.common.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)  //只作用于方法之上
@Retention(RetentionPolicy.RUNTIME)  //在运行时生效
public @interface TokenVerify {
    /**
     * 4代表管理员
     * 3代表审核员
     * 2代表发布员
     * 1代表普通用户
     * @return
     */
}