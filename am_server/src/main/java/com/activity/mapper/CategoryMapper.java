package com.activity.mapper;

import com.activity.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface CategoryMapper extends BaseMapper<Category> {
}
