package com.activity.mapper;


import com.activity.entity.Aclog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AclogMapper extends BaseMapper<Aclog> {
}
