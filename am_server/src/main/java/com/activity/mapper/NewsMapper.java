package com.activity.mapper;

import com.activity.entity.News;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;
import java.util.List;

public interface  NewsMapper extends BaseMapper<News> {
    //多表查询成功,根据用户名查询到用户所属部门的活动(当前用户的权限是审核员)
    @Select("select * from news where news.Departmentid=(select department.ID from user,department where department.ID=user.Departmentid and user.Username=#{username})")
    List<News> getAllNewsByUser(@PathVariable String username);

    //审核功能
    @Update("update news set news.State=1 where ID=#{id}")
    boolean examine(@PathVariable Integer id);

    //驳回功能
    @Update("update news set news.State=3 where ID=#{id}")
    boolean reject(@PathVariable Integer id);

    //点击量
    @Update("update news set news.Clicks=news.Clicks+1 where ID=#{id}")
    boolean clicks(@PathVariable Integer id);

    @Select("SELECT * FROM news WHERE news.State=1 AND UNIX_TIMESTAMP(Startdate) <= UNIX_TIMESTAMP(#{targetDate}) AND UNIX_TIMESTAMP(Enddate) >= UNIX_TIMESTAMP(#{targetDate})")
    List<News> selectByDate(Date targetDate);

}
