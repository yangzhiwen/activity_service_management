package com.activity.mapper;

import com.activity.entity.Department;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface DepartmentMapper extends BaseMapper<Department> {
}
