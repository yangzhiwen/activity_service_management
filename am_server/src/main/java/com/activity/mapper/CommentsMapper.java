package com.activity.mapper;

import com.activity.entity.Comments;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface CommentsMapper extends BaseMapper<Comments> {
}
