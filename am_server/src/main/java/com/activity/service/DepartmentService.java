package com.activity.service;

import com.activity.entity.Department;
import com.activity.entity.News;

import java.util.List;

public interface DepartmentService {
    //添加一个部门
    boolean insert(Department department);

    //删除一个部门
    boolean delete(Integer ID);

    //查询所有部门
    List<Department> selectAll();

//    //修改部门
//    boolean update(Department department);

    //保存用户
    boolean save(Department department);



}
