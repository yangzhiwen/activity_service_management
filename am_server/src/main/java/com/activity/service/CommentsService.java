package com.activity.service;

import com.activity.entity.Comments;

import java.util.List;

public interface CommentsService {
    //添加一个用户
    boolean insert(Comments comments);

    //删除一个用户
    boolean delete(Integer ID);

    //查询所有用户
    List<Comments> selectAll();

    //修改用户
    boolean update(Comments comments);

    //根据活动id查评论
    List<Comments> selectById(Integer id);

}
