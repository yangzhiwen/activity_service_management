package com.activity.service;

import com.activity.entity.Aclog;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface AclogService {
    //插入日志信息
    boolean insertLog(Aclog aclog);

    //查询操作,显示所有日志
    List<Aclog> selectAll();

    //分页功能实现
    List<Aclog> selectPage(@PathVariable Integer current,@PathVariable Integer size);

    //删除这个日志
    boolean deleteLog(Aclog aclog);

    //条件查询带分页的方法



}

