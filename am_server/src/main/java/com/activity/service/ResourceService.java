package com.activity.service;

import com.activity.entity.Resourceurl;

import java.util.List;

public interface ResourceService {
    /**
     * Resource资源类
     * @return
     */
    //插入图片
    boolean insertPicture(Resourceurl resourceurl);
    //插入视频
    boolean insertVideo(Resourceurl resourceurl);
    //插入文字
    boolean insertWord(Resourceurl resourceurl);
    //显示所有信息
    List<Resourceurl> showAll();

    //根据活动id查询活动资源
    List<Resourceurl> selectByNewsId(Integer id);

    //根据主键id查找活动
    Resourceurl selectById(Integer id);

    //根据id删除活动
    boolean deleteById(Integer id);
}