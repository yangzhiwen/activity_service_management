package com.activity.service;

import com.activity.entity.News;
import com.activity.entity.User;
import io.swagger.models.auth.In;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface NewsService {
    //添加一个活动信息
    boolean insert(News news);

    //删除一个活动信息
    boolean delete(Integer ID);

    //查询所有活动信息
    List<News> selectAll();

    //修改活动信息
    boolean update(News news);

    //根据活动标题做一个模糊查询
    List<News> searchByName(String title);

    //保存用户
    boolean save(News news);
//
    //审核
    boolean examine(Integer id);

    //根据日查询
    List<News> selectByDate(Date targetDate);
    //根据月份查询
    Map<String, List<News>> selectByMonthDate(String month);


}
