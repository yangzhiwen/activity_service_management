package com.activity.service;

import com.activity.entity.Category;
import com.activity.entity.News;

import java.util.List;

public interface CategoryService {
    //添加一个用户
    boolean insert(Category category);

    //删除一个用户
    boolean delete(Integer ID);

    //查询所有用户
    List<Category> selectAll();

    //修改用户
    boolean update(Category category);

    //保存用户
    boolean save(Category category);
}
