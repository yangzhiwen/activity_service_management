package com.activity.service.impl;

import com.activity.entity.Comments;
import com.activity.mapper.CommentsMapper;
import com.activity.service.CommentsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class CommentsServiceImpl implements CommentsService {
    @Resource
    private CommentsMapper commentsMapper;

    @Override
    public boolean insert(Comments comments) {
        comments.setCommdate(new Date());
        int insert = commentsMapper.insert(comments);
        return insert!=0;
    }

    @Override
    public boolean delete(Integer ID) {
        QueryWrapper<Comments> wrapper = new QueryWrapper<>();
        wrapper.eq("id",ID);
        int delete = commentsMapper.delete(wrapper);
        return delete!=0;
    }

    @Override
    public List<Comments> selectAll() {
        List<Comments> comments = commentsMapper.selectList(null);
        return comments;
    }

    @Override
    public boolean update(Comments comments) {
        QueryWrapper<Comments> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id",comments.getId());
        int update = commentsMapper.update(comments, queryWrapper);
        return update!=0;
    }

    @Override
    public List<Comments> selectById(Integer id) {
        QueryWrapper<Comments> wrapper = new QueryWrapper<>();
        wrapper.eq("newsid",id);
        List<Comments> commentsList = commentsMapper.selectList(wrapper);
        return commentsList;
    }


}
