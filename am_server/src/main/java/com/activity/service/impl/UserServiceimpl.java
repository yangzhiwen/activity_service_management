package com.activity.service.impl;

import com.activity.common.result.Result;
import com.activity.entity.User;
import com.activity.mapper.UserMapper;
import com.activity.service.UserService;
import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceimpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public User login(User user) {
        String username=user.getUsername();
        String password=user.getPassword();
        QueryWrapper<User> login1 = new QueryWrapper<User>().eq("Username", username).eq("Password",password);
        User user1 = userMapper.selectOne(login1);
        return user1;
    }

    //增加一个用户
    public boolean insert(User user){
        int insert = userMapper.insert(user);
        return insert!=0;
    }

    //根据姓名删除用户
    @Override
    public boolean delete(Integer id) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("Userid",id);
//        System.out.println("impl里面的id"+id);
        int delete = userMapper.delete(wrapper);
        return delete!=0;
    }

    @Override
    public boolean reset(Integer id) {

        User user=new User();
        user.setUserid(id);
        user.setPassword("ha12345");
        int i = userMapper.updateById(user);
        return i!=0;
    }

    //查询所有用户
    @Override
    public List<User> selectAll() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.ne("username","admin");

        List<User> users = userMapper.selectList(wrapper);
        return users;
    }

    //根据姓名修改用户
    @Override
    public boolean update( User user) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("Username",user.getUsername());
        int update = userMapper.update(user, queryWrapper);
        return update!=0;
    }

    @Override
    public boolean save(User user) {
        return false;
    }

    /**
     * 先登录，如果登陆失败，则显示旧密码输入错误
     * 登录成功，更改密码1
     * @param username
     * @param oldPwd
     * @return
     */
    @Override
    public boolean updatePWd(String username, String oldPwd) {
        QueryWrapper<User> login1 = new QueryWrapper<User>().eq("Username", username).eq("Password",oldPwd);
        User user1 = userMapper.selectOne(login1);
        return user1!=null;
    }

    @Override
    public User selectUserLevel(User user) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username",user.getUsername());
        User user1 = userMapper.selectOne(wrapper);
        return user1;
    }


    public User selectUsername(String username) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        User user1 = userMapper.selectOne(wrapper);
        return user1;
    }
}
