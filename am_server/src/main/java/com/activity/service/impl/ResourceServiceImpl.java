package com.activity.service.impl;

import com.activity.entity.Resourceurl;
import com.activity.mapper.ResourceMapper;
import com.activity.service.ResourceService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;

@Service
public class ResourceServiceImpl implements ResourceService {
    @Resource
    private ResourceMapper resourceMapper;


    @Override
    public boolean insertPicture(Resourceurl resourceurl) {
        int insert = resourceMapper.insert(resourceurl);
        return insert!=0;
    }

    @Override
    public boolean insertVideo(Resourceurl resourceurl) {
        int insert = resourceMapper.insert(resourceurl);
        return insert!=0;
    }

    @Override
    public boolean insertWord(Resourceurl resourceurl) {
        int insert = resourceMapper.insert(resourceurl);
        return insert!=0;
    }

    @Override
    public List<Resourceurl> showAll() {
        List<Resourceurl> resourceurls = resourceMapper.selectList(null);
        return resourceurls;
    }

    @Override
    public List<Resourceurl> selectByNewsId(Integer id) {
        QueryWrapper<Resourceurl> wrapper = new QueryWrapper<>();
        wrapper.eq("newsid", id);
        List<Resourceurl> resourceurls = resourceMapper.selectList(wrapper);
        return resourceurls;
    }

    @Override
    public Resourceurl selectById(Integer id) {
        Resourceurl resourceurl = resourceMapper.selectById(id);
        return resourceurl;
    }

    @Override
    public boolean deleteById(Integer id) {
        Resourceurl selectById = selectById(id);
        new File(selectById.getResourceurl()).delete();
        int i = resourceMapper.deleteById(id);
        return i>0;
    }

}
