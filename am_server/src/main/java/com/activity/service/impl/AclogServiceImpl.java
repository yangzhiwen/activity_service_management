package com.activity.service.impl;

import com.activity.entity.Aclog;
import com.activity.mapper.AclogMapper;
import com.activity.service.AclogService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AclogServiceImpl implements AclogService {
    @Resource
    private AclogMapper logMapper;

    @Override
    public boolean insertLog(Aclog aclog) {
        int insert = logMapper.insert(aclog);
        return insert!=0;
    }

    @Override
    public List<Aclog> selectAll() {
        List<Aclog> aclogs = logMapper.selectList(null);
        return aclogs;
    }

    //分页查询
    public List<Aclog> selectPage(@PathVariable Integer current,@PathVariable Integer size){
        Page<Aclog> page=new Page<>(current,size);
        IPage<Aclog> aclogIPage = logMapper.selectPage(page, null);
//        System.out.println("分页茶粗impl"+page.getRecords());
        List<Aclog> records = page.getRecords();
        return records;
    }

    //删除这个日志
    public boolean deleteLog(@RequestBody Aclog aclog){
        int i = logMapper.deleteById(aclog.getId());
        return i!=0;
    }
}
