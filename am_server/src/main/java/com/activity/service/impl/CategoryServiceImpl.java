package com.activity.service.impl;

import com.activity.entity.Category;
import com.activity.entity.News;
import com.activity.mapper.CategoryMapper;
import com.activity.service.CategoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Resource
    private CategoryMapper categoryMapper;

    @Override
    public boolean insert(Category category) {
        int insert = categoryMapper.insert(category);
        return insert!=0;
    }

    @Override
    public boolean delete(Integer ID) {
        QueryWrapper<Category> wrapper = new QueryWrapper<>();
        wrapper.eq("ID",ID);
        int delete = categoryMapper.delete(wrapper);
        return delete!=0;
    }

    @Override
    public List<Category> selectAll() {
        List<Category> categories = categoryMapper.selectList(null);
        return categories;
    }

    @Override
    public boolean update(Category category) {
        QueryWrapper<Category> wrapper = new QueryWrapper<>();
        wrapper.eq("ID",category.getId());
        int update = categoryMapper.update(category, wrapper);
        return update!=0;
    }
    @Override
    public boolean save(Category category) {
        if(category.getId() == null){
//            System.out.println(category);
            return categoryMapper.insert(category) != 0;
        }
        return categoryMapper.updateById(category) != 0;
    }
}
