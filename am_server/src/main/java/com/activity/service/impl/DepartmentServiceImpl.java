package com.activity.service.impl;

import com.activity.entity.Department;
import com.activity.entity.News;
import com.activity.mapper.DepartmentMapper;
import com.activity.service.DepartmentService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Resource
    private DepartmentMapper departmentMapper;

    //添加部门
    @Override
    public boolean insert(Department department) {
        int insert = departmentMapper.insert(department);
        return insert!=0;
    }

    @Override
    public boolean save(Department department) {
        if(department.getId() == null){
            return insert(department);
        }
        return departmentMapper.updateById(department) != 0;
    }

    //删除部门
    @Override
    public boolean delete(Integer ID) {
        QueryWrapper<Department> wrapper = new QueryWrapper<>();
        wrapper.eq("ID",ID);
        int delete = departmentMapper.delete(wrapper);
        return delete!=0;
    }

    //显示所有部门
    @Override
    public List<Department> selectAll() {
        List<Department> departments = departmentMapper.selectList(null);
        return departments;
    }

//    @Override
//    public boolean update(Department department) {
//        QueryWrapper<Department> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("Dname",department.getDname());
//        int update = departmentMapper.update(department, queryWrapper);
//        return update!=0;
//    }
}
