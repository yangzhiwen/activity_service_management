package com.activity.service;

import com.activity.entity.User;

import java.util.List;

public interface UserService {
    //用户登录
    User login(User user);

    //添加一个用户
    boolean insert(User user);

    //删除一个用户
    boolean delete(Integer id);

    //重置密码
    boolean reset(Integer id);
    //查询所有用户
    List<User> selectAll();

    //修改用户
    boolean update(User user);

    //保存用户
    boolean save(User user);

    /**
     * 前端发来三个参数
     * 用户名
     * 旧密码
     * 新密码
     */
    boolean updatePWd(String username,String oldPwd);


    //根据用户名查询用户的等级
    User selectUserLevel(User user);


}
