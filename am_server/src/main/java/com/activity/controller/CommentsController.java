package com.activity.controller;

import com.activity.common.aop.OperLog;
import com.activity.common.result.Result;
import com.activity.common.util.TokenVerify;
import com.activity.entity.Comments;
import com.activity.service.impl.CommentsServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api
@RestController
@RequestMapping("/comments")
public class CommentsController {
    @Autowired
    private CommentsServiceImpl commentsService;


    //添加一个部门
    @TokenVerify()
    @PostMapping("/insert")
//    @OperLog(operModul = "评论模块",operType = "添加",operDesc = "添加一个评论")
    public Result insert(@RequestBody Comments comments){
        boolean insert = commentsService.insert(comments);
        if (insert){
            return Result.success("添加活动成功");
        }else{
            return Result.fail("添加失败");

        }
    }

    //查询所有评论信息
//    @OperLog(operModul = "评论模块",operType = "查询",operDesc = "查询所有评论")
    @TokenVerify()
    @GetMapping("/selectAll")
    public Result selectAll(){
        List<Comments> commentsList = commentsService.selectAll();
        return Result.success("查询成功",commentsList);
    }

    //删除评论信息
    @TokenVerify()
    @DeleteMapping("/delete/{id}")
//    @OperLog(operModul = "评论模块",operType = "删除",operDesc = "删除评论")
    public Result delete(@PathVariable Integer id){
        boolean delete = commentsService.delete(id);
        if (delete){
            return Result.success("删除成功");
        }else {
            return Result.fail("删除失败");
        }
    }

    //更改公告信息
    @TokenVerify()
    @PostMapping("/update")
//    @OperLog(operModul = "评论模块",operType = "更改",operDesc = "更改一个评论")
    public Result update(@RequestBody Comments comments){
        boolean update = commentsService.update(comments);
        if (update){
            return Result.success("更改成功");
        }else{
            return Result.fail("更改失败");
        }
    }
    //根据活动id查询评论
    @TokenVerify()
     @GetMapping("/selectCommentsById/{id}")
    public Result selectCommentsById(@PathVariable Integer id){
         List<Comments> commentsList = commentsService.selectById(id);
         if (commentsList.size()>0){
             return Result.success("查询评论成功",commentsList);
         }else {
             return Result.fail("查询失败");

         }
     }
}
