package com.activity.controller;

import com.activity.common.aop.OperLog;

import com.activity.common.constact.UserLevel;
import com.activity.common.result.Result;
import com.activity.common.util.RequestUtils;
import com.activity.common.util.TokenVerify;
import com.activity.entity.News;
import com.activity.entity.NewsSearchCondition;
import com.activity.entity.User;
import com.activity.mapper.NewsMapper;
import com.activity.service.impl.NewsServiceImpl;
import com.activity.service.impl.UserServiceimpl;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.PublicKey;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Api
@RestController
@RequestMapping("/news")
public class NewsController {
    @Autowired
    private NewsServiceImpl newsService;
    @Autowired
    private UserServiceimpl userServiceimpl;

    //添加活动信息
    @TokenVerify()
    @PostMapping("/insert")
    public Result insert(@RequestBody News news) {

        news.setAuthor(RequestUtils.getCurrentUsername());
        boolean insert = newsService.insert(news);
        if (insert) {
            return Result.success("添加活动成功");
        } else {
            return Result.fail("添加失败");
        }
    }

    //查询所有公告信息
    @TokenVerify()
    @GetMapping("/selectAll")
    public Result selectAll() {
        Integer userLevel = RequestUtils.getCurrentUserLevel();
        String username = RequestUtils.getCurrentUsername();
        List<News> news = null;
        if (userLevel.equals(UserLevel.NEWS_ADD)) {
            news = newsService.selectByAuthor(username);
        } else if (userLevel.equals(UserLevel.NEWS_EXAMINE_USER)) {
            User user = userServiceimpl.selectUsername(username);
            news = newsService.selectByDeptAndNotExamine(user.getDepartmentid());
        }else if(userLevel.equals(UserLevel.ADMIN)){
            news = newsService.selectAll();
        }
        else {
            news = newsService.selectByEnableNews();
        }
        return Result.success("查询成功", news);
    }



    //删除公告信息
    @TokenVerify()
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        boolean delete = newsService.delete(id);
        if (delete) {
            return Result.success("删除成功");
        } else {
            return Result.fail("删除失败");
        }
    }

    //更改公告信息
    @TokenVerify()
    @PostMapping("/update")
    public Result update(@RequestBody News news) {
        boolean update = newsService.update(news);
        if (update) {
            return Result.success("更改成功");
        } else {
            return Result.fail("更改失败");
        }
    }

    //保存公告信息
    @TokenVerify()
    @PostMapping("/save")
    public Result save(@RequestBody News news) {
        boolean save = newsService.save(news);
        if (save) {
            return Result.success("保存成功");
        } else {
            return Result.fail("保存失败");
        }
    }

    @TokenVerify()
    @GetMapping("/hello")
    public Result hello() {

            return Result.success("查询成功");

    }

    //根据标题进行模糊查询
    @TokenVerify()
    @GetMapping("/searchByName/{title}")
    public Result search(@PathVariable String title) {
        List<News> news = newsService.searchByName(title);
        if (news.size() > 0) {
            return Result.success("查询成功", news);
        } else {
            return Result.fail("查询失败");
        }
    }

    @Resource
    private NewsMapper newsMapper;

    //根据姓名所在的部门,显示所有活动
    @TokenVerify()
    @GetMapping("/showAllByName/{name}")
    public Result showAll(@PathVariable String name) {
        List<News> news = newsMapper.getAllNewsByUser(name);
        if (news.size() > 0) {
            return Result.success("显示成功", news);
        } else {
            return Result.fail("查询失败,没有记录");
        }
    }

    //审核功能
    @GetMapping("/examine/{id}")
    @TokenVerify()
    public Result examine(@PathVariable Integer id) {
        boolean examine = newsMapper.examine(id);
        if (examine) {
            return Result.success("审核成功");
        } else {
            return Result.fail("审核失败");
        }
    }

    //驳回功能
    @GetMapping("/reject/{id}")
    @TokenVerify()
    public Result reject(@PathVariable Integer id) {
        boolean examine = newsMapper.reject(id);
        return Result.success("操作成功");
    }
    //点击量增加
    @GetMapping("/clicks/{id}")
    @TokenVerify()
    public Result clicks(@PathVariable Integer id) {
        boolean examine = newsMapper.clicks(id);
        return Result.success("操作成功");
    }

    /**
     * @param targetDate
     * @return
     */
    @GetMapping("/selectByDate/{targetDate}")
    @TokenVerify()
    public Result selectByDate(@PathVariable Date targetDate) {
        List<News> news = newsMapper.selectByDate(targetDate);
        if (news.size() > 0) {
            return Result.success("查询成功", news);
        } else {
            return Result.fail("查询失败");
        }
    }

    @GetMapping("/selectByMonthDate/{date}")
    @TokenVerify()
    public Result selectByMonthDate(@PathVariable("date") String date) {
        Map<String, List<News>> news = newsService.selectByMonthDate(date);
        return Result.success("查询成功", news);
    }

    @TokenVerify()
    @RequestMapping("/selectByMonthDate")
    public Result selectByMonthDate(@RequestBody JSONObject jsonObject) {
        Integer departmentId =  jsonObject.getInteger("departmentId");
        String date = jsonObject.getString("date");
        Map<String, List<News>> news = newsService.selectByMonthDate(departmentId, date);
        return Result.success("查询成功", news);
    }

    @TokenVerify()
    @PostMapping("/searchByCondition")
    public Result searchByCondition(@RequestBody NewsSearchCondition newsSearchCondition) {
        Map<String, List<News>> news = newsService.searchByCondition(newsSearchCondition);
        return Result.success("查询成功", news);
    }

}
