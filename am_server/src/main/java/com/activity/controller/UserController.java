package com.activity.controller;

import com.activity.common.aop.OperLog;
import com.activity.common.result.Result;
import com.activity.common.util.JwtUtil;
import com.activity.entity.Aclog;
import com.activity.entity.User;
import com.activity.service.impl.AclogServiceImpl;
import com.activity.service.impl.UserServiceimpl;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Api
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private AclogServiceImpl aclogService;

    @PostMapping("/addLog")
    public Result login(@RequestBody Aclog aclog) {
        boolean b = aclogService.insertLog(aclog);
        if (b) {
            return Result.success("成功", aclog);
        } else {
            return Result.fail("失败", aclog);
        }

    }


    @Autowired
    private UserServiceimpl userServiceimpl;

    // 登录功能
    @PostMapping("/login")
//    @OperLog(operModul = "用户模块",operType = "登录",operDesc = "登录功能")
    public Result login(@RequestBody User user, HttpServletRequest request) {
        User loginUser = userServiceimpl.login(user);
        if (loginUser != null) {
            loginUser.setPassword("*******");

            String sign = JwtUtil.sign(user.getUsername(), loginUser.getUserlevel());


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userData", loginUser);
            jsonObject.put("token", sign);

            return Result.success("登录成功", jsonObject);
        }else{
            return Result.fail("登陆失败");
        }




}

    //更改密码123
    @PostMapping("/updatePwd/{username}/{oldPwd}/{newPwd}")
    public Result updatePwd(@PathVariable String username, @PathVariable String oldPwd, @PathVariable String newPwd) {
        //先做登录
//        boolean b = userServiceimpl.updatePWd(username, oldPwd);
        if (oldPwd.equals(newPwd)) {//登录成功了
            User user = new User();
            user.setUsername(username);
            user.setPassword(newPwd);
            userServiceimpl.update(user);
            return Result.success("更改密码成功");
        } else {//旧密码错误，不能登陆
            return Result.fail("密码错误，请重新输入");
        }
    }

    //新增一个活动
    @PostMapping("/insert")
//    @OperLog(operModul = "用户模块",operType = "增加",operDesc = "添加了一个用户")
    public Result insert(@RequestBody User user) {
        boolean insert = userServiceimpl.insert(user);
        if (insert) {
            return Result.success("增加用户成功");
        } else {
            return Result.fail("添加用户失败", user);
        }
    }

    //删除一个用户
    @PostMapping("/deleteByID/{ID}")
//    @OperLog(operModul = "用户模块",operType = "删除",operDesc = "删除一个用户")
    public Result delete(@PathVariable Integer ID) {
        boolean delete = userServiceimpl.delete(ID);
        if (delete) {
            return Result.success("删除用户成功");
        } else {
            return Result.fail("删除用户失败");
        }
    }
    @PostMapping("/resetUserUpdate/{ID}")
    public Result resetUserUpdate(@PathVariable Integer ID) {
        boolean reset = userServiceimpl.reset(ID);
        if (reset) {
            return Result.success("重置用户成功");
        } else {
            return Result.fail("重置用户失败");
        }
    }

    //查询所有用户

    @GetMapping("/selectAll")
//    @OperLog(operModul = "用户模块",operType = "查询",operDesc = "查询所有一个用户")
    public Result selectAll(HttpServletRequest request) {

        List<User> users = userServiceimpl.selectAll();
        return Result.success("查询所有用户成功", users);
    }

    //根据用户姓名更改用户信息
    @PostMapping("/updateByName")
//    @OperLog(operModul = "用户模块",operType = "更改",operDesc = "更改了一个用户")
    public Result updateUserByName(@RequestBody User user) {
        boolean update = userServiceimpl.update(user);
        if (update) {
            return Result.success("修改成功");
        } else {
            return Result.fail("修改失败");
        }
    }


}
