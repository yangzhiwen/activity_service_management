package com.activity.controller;

import com.activity.common.aop.OperLog;
import com.activity.common.result.Result;
import com.activity.common.util.TokenVerify;
import com.activity.entity.Aclog;
import com.activity.service.impl.AclogServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api
@RestController
@RequestMapping("/log")
public class AclogController {
    @Autowired
    private AclogServiceImpl aclogService;


    //查询所有日志
    @TokenVerify()
    @GetMapping("/findAll")
    public Result findAll(){
        List<Aclog> aclogs = aclogService.selectAll();
        if (aclogs.size()>0){
            return Result.success("查询成功",aclogs);
        }else {
            return Result.fail("查询失败");
        }
    }

    //分页接口
    /**
     * new page对象,传入两个参数,当前页和每页记录数
     * 调用mp方法实现分页查询
     * */
    @TokenVerify()
    @GetMapping("/findAllPage/{current}/{size}")
    public Result findPage(@PathVariable Integer current, @PathVariable Integer size){
        List<Aclog> aclogs = aclogService.selectPage(current, size);
        if(aclogs.size()>0){
            return Result.success("分页查询成功",aclogs);
        }else{
            return Result.fail("分页查询失败");
        }
    }

    //根据id删除这个日志
    @TokenVerify()
    @PostMapping("/deleteLogById")
    public Result deleteLog(@RequestBody Aclog aclog){
        boolean b = aclogService.deleteLog(aclog);
        if (b){
            return Result.success("删除日志成功");
        }else {
            return Result.fail("删除日志失败");
        }
    }

}
