package com.activity.controller;

import com.activity.common.aop.OperLog;
import com.activity.common.result.Result;
import com.activity.common.util.TokenVerify;
import com.activity.entity.Category;
import com.activity.entity.News;
import com.activity.service.impl.CategoryServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryServiceImpl categoryService;

    //添加一个部门
    @TokenVerify()
    @PostMapping("/insert")
//    @OperLog(operModul = "活动类型模块",operType = "添加",operDesc = "添加一个活动类型")
    public Result insert(@RequestBody Category category){
        boolean insert = categoryService.insert(category);
        if (insert){
            return Result.success("添加活动成功");
        }else{
            return Result.fail("添加失败");
        }
    }

    //查询所有公告信息
    @TokenVerify()
    @GetMapping("/selectAll")
//    @OperLog(operModul = "活动类型模块",operType = "查询",operDesc = "查询所有活动类型")
    public Result selectAll(){
        List<Category> categories = categoryService.selectAll();
        return Result.success("查询成功",categories);
    }

    //删除公告信息
    @TokenVerify()
    @DeleteMapping("/delete/{id}")
//    @OperLog(operModul = "活动类型模块",operType = "删除",operDesc = "删除一个活动类型")
    public Result delete(@PathVariable Integer id){
        boolean delete = categoryService.delete(id);
        if (delete){
            return Result.success("删除成功");
        }else {
            return Result.fail("删除失败");
        }
    }

    //更改公告信息
    @TokenVerify()
    @PostMapping("/update")
//    @OperLog(operModul = "活动类型模块",operType = "更改",operDesc = "更改所有活动类型")
    public Result update(@RequestBody Category category){
        boolean update = categoryService.update(category);
        if (update){
            return Result.success("更改成功");
        }else{
            return Result.fail("更改失败");
        }
    }
    @TokenVerify()
    @PostMapping("/save")
    public Result save(@RequestBody Category category){
        boolean save = categoryService.save(category);
        if (save){
            return Result.success("保存成功");
        }else{
            return Result.fail("保存失败");
        }
    }
}
