package com.activity.controller;

import com.activity.common.aop.OperLog;
import com.activity.common.result.Result;
import com.activity.common.util.TokenVerify;
import com.activity.entity.Department;
import com.activity.entity.News;
import com.activity.service.impl.DepartmentServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api
@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private DepartmentServiceImpl departmentService;


//    /**
//     *
//     * @param department
//     * @return
//     */
//    @PostMapping("/insert")
//    @OperLog(operModul = "部门模块",operType = "添加",operDesc = "添加部门")
//    public Result insert(@RequestBody Department department){
//        boolean insert = departmentService.insert(department);
//        if (insert){
//            return Result.success("添加活动成功");
//        }else{
//            return Result.fail("添加失败");
//        }
//    }

    //查询所有公告
    @TokenVerify()
    @GetMapping("/selectAll")
//    @OperLog(operModul = "部门模块",operType = "查询",operDesc = "查询所有部门信息")
    public Result selectAll(){
        List<Department> departments = departmentService.selectAll();
        return Result.success("查询成功",departments);
    }

    //删除公告信息
    @TokenVerify()
    @DeleteMapping("/delete/{id}")
//    @OperLog(operModul = "部门模块",operType = "删除",operDesc = "删除部门")
    public Result delete(@PathVariable Integer id){
        boolean delete = departmentService.delete(id);
        if (delete){
            return Result.success("删除成功");
        }else {
            return Result.fail("删除失败");
        }
    }

//    //更改部门信息
//    @PostMapping("/update")
//    @OperLog(operModul = "部门模块",operType = "更改",operDesc = "更改部门信息")
//    public Result update(@RequestBody Department department){
//        boolean update = departmentService.update(department);
//        if (update){
//            return Result.success("更改成功");
//        }else{
//            return Result.fail("更改失败");
//        }
//    }

    //保存部门信息
    @TokenVerify()
    @PostMapping("/save")
//    @OperLog(operModul = "部门模块",operType = "save功能",operDesc = "save部门")
    public Result save(@RequestBody Department department){
        boolean save = departmentService.save(department);
        if (save){
            return Result.success("保存成功");
        }else{
            return Result.fail("保存失败");
        }
    }

}
