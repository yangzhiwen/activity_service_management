package com.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
public class User {
    @TableId(type = IdType.AUTO)
    private Integer Userid;

    public Integer getUserid() {
        return Userid;
    }

    public void setUserid(Integer userid) {
        Userid = userid;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public Integer getDepartmentid() {
        return Departmentid;
    }

    public void setDepartmentid(Integer departmentid) {
        Departmentid = departmentid;
    }

    public Integer getUserlevel() {
        return Userlevel;
    }

    public void setUserlevel(Integer userlevel) {
        Userlevel = userlevel;
    }

    public Integer getState() {
        return State;
    }

    public void setState(Integer state) {
        State = state;
    }

    private String Username;

    private String Password;

    private Integer Departmentid;

    private Integer Userlevel;

    private Integer State;

    public Integer getL(){
        return this.Userlevel;
    }


}
