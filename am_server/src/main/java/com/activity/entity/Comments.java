package com.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comments {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String Commentator;
    private Integer Newsid;
    private Date Commdate;
    private String Content;
}
