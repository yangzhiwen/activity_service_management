package com.activity.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @version V1.0
 * @Package com.activity.entity
 * @auhter 枯木Kum
 * @date 2021/5/22-2:19 PM
 * <p>...</p>
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class NewsSearchCondition {

    /**
     * 开始时间
     */
    private Date startDate;
    /**
     * 结束时间
     */
    private Date endDate;
    /**
     * 具体时间限制(星期)
     */
    private List<Integer> weeks;
    /**
     * 活动标题
     */
    private String newsTitle;
    /**
     * 活动内容
     */
    private String newsContent;
    /**
     * 发布人
     */
    private String author;
    /**
     * 活动类型
     */
    private Integer categoryId;
    /**
     * 合作方
     */
    private String partner;
    /**
     * 关键字
     */
    private String keyword;


    /**
     * 部门ID
     */
    private Integer deptId;



}
