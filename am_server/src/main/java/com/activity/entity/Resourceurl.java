package com.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Resourceurl {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer newsid;

    private String type;

    private String resourceurl;


}
