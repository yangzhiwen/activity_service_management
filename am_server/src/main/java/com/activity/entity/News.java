package com.activity.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class News {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String Title;
    private String Content;
    private String Author;
    private Date Time;
    private Integer Categoryid;
    private String Partner;
    private String Keyword;
    private Date Startdate;
    private Date Enddate;
    private String Activeday;
    private Integer Clicks = 0;
    private Integer State;
    private Integer Departmentid;

    public List<Integer> getWeekCondition() {
        try {
            JSONObject object = JSON.parseObject(this.Activeday);
            JSONArray week = object.getJSONArray("week");
            return week.toJavaList(Integer.class);

        } catch (JSONException e) {
            return new ArrayList<>();
        }
    }


}
